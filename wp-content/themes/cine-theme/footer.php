		<footer>
			<section class="container">
				<ul class="cats">
					<?php 
						$args = array(
								'title_li' => ''
							);
						wp_list_categories($args); 
					?>
				</ul>
				<?php get_template_part('includes/build/build', 'social'); ?>
			</section>
		</footer>
	</section>
</section>
</body>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/lib/jquery-1.8.2.min.js" type="text/javascript"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/production.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/js/font-awesome/css/font-awesome.min.css">
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-35663434-2', 'auto');
  ga('send', 'pageview');

</script>
<?php wp_footer() ?>
</html>
