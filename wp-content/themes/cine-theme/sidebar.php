<?php if(is_home()) { ?>
	<section class="sidebar-content">
		<h2>About CineResource</h2>
		<?php
			global $post;
			$args = array( 
				'numberposts' 	=> 1,
				'post_type'		=> 'page'
			);
			$myposts = get_posts( $args );
			foreach( $myposts as $post ) :
				setup_postdata($post); 
		?>
			<?php the_content(); ?>
		<?php endforeach; 
		wp_reset_postdata(); ?>
	</section>
	<section class="sidebar-content">
		<h2>Writers</h2>
		<ul>
			<?php
		        $blogusers = get_users_of_blog();
		        if ($blogusers) {
		          foreach ($blogusers as $bloguser) {
		            $user = get_userdata($bloguser->user_id);
			?>
		    <li>
		        <aside>
			        <section class="author-picture">            
			            <?php echo get_avatar( $user->ID, 80 ); ?>
			        </section>
		        </aside>
		        <aside>
			        <h3><?php echo $user->user_firstname; ?> <?php echo $user->user_lastname; ?></h3>
			        <p><?php echo $user->user_description; ?></p>
		        </aside>
		    </li>
		    <?php
		    	wp_reset_query();  // Restore global post data stomped by the_post().
		          }
		        }
		    ?>
	    </ul>
	</section>
<?php } ?>
<?php if(is_category()) { ?>
	<section class="sidebar-content">
		<h2><?php single_cat_title( '', true ); ?></h2>
		<?php echo category_description(); ?>
	</section>
	<section class="sidebar-content">
		<h2>Writers</h2>
		<ul>
			<?php
		        $blogusers = get_users_of_blog();
		        if ($blogusers) {
		          foreach ($blogusers as $bloguser) {
		            $user = get_userdata($bloguser->user_id);
			?>
		    <li>
		        <aside>
			        <section class="author-picture">            
			            <?php echo get_avatar( $user->ID, 80 ); ?>
			        </section>
		        </aside>
		        <aside>
			        <h3><?php echo $user->user_firstname; ?> <?php echo $user->user_lastname; ?></h3>
			        <p><?php echo $user->user_description; ?></p>
		        </aside>
		    </li>
		    <?php
		    	wp_reset_query();  // Restore global post data stomped by the_post().
		          }
		        }
		    ?>
	    </ul>
	</section>
<?php } ?>
<?php if(is_single()) { ?>
	<section class="sidebar-content extra-space">
		<h2>Written by</h2>
		<ul>
			<li>
				<aside>
					<?php echo get_avatar( get_the_author_meta( 'ID' ), 80 ); ?>
				</aside>
				<aside>
					<h3><?php the_author_meta('display_name'); ?></h3>
                    <p><?php the_author_description(); ?></p>
				</aside>
			</li>
		</ul>
	</section>
<?php } ?>
<section class="sidebar-content">
	<h2>Recent Articles</h2>
	<ul>
		<?php
			global $post;
			$args = array( 
				'numberposts' => 5
			);
			$myposts = get_posts( $args );
			foreach( $myposts as $post ) :
				setup_postdata($post); 
		?>
			<li>
				<aside>
					<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail('thumbnail'); ?></a>
				</aside>
				<aside>
					<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
					<p>
						<span><i class="fa fa-pencil"></i> <?php the_author(); ?></span> 
						<span><i class="fa fa-calendar-o"></i> <?php the_time('d.m.y'); ?></span>
					</p>
				</aside>
			</li>
		<?php endforeach; 
		wp_reset_postdata(); ?>
	</ul>
</section>
<section class="sidebar-content">
	<h2>Most Commented</h2>
	<ul>
		<?php
			global $post;
			$args = array( 
				'numberposts' 	=> 5,
				'orderby'		=> 'comment_count'
			);
			$myposts = get_posts( $args );
			foreach( $myposts as $post ) :
				setup_postdata($post); 
		?>
			<li>
				<aside>
					<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail('thumbnail'); ?></a>
				</aside>
				<aside>
					<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
					<p>
						<span><i class="fa fa-comments"></i> <?php comments_number( 'no comments', 'one comment', '% comments' ); ?></span>
					</p>
				</aside>
			</li>
		<?php endforeach; 
		wp_reset_postdata(); ?>
	</ul>
</section>
