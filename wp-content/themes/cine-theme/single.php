<?php get_header(); ?>
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<?php
		$feature = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
	?>
	<section class="feature" style="background-image: url('<?php echo $feature[0]; ?>');"></section>
	<section class="container">
		<!--Start Post List-->
			<aside class="post-article">
				<!--Start Post Content-->
					<article>
						<section class="article-share">
							<ul>
								<li><a href="http://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>&p[images][0]=<?php echo $feature[0];?>'" target="_blank"><i class="fa fa-facebook fa-2x"></i></a></li>
								<li><a href="https://twitter.com/intent/tweet?text=<?php the_title(); ?>%20via%20@cineresource&url=<?php the_permalink(); ?>" target="_blank"><i class="fa fa-twitter fa-2x"></i></a></li>
								<li><a href="https://plus.google.com/share?url=<?php the_permalink(); ?>" target="_blank"><i class="fa fa-google-plus fa-2x"></i></a></li>
							</ul>
						</section>
						<section class="article-content">
							<h1><?php the_title(); ?></h1>
							<h3 class="icons">
								<span><i class="fa fa-pencil"></i> <?php the_author(); ?></span>
								<span><i class="fa fa-calendar-o"></i> <?php the_time('d.m.y'); ?></span>
							</h3>
							<section class="single-article-content">
								<?php the_content(); ?>
							</section>
							<section class="article-meta">
								<h3 class="icons">
									<span><i class="fa fa-folder-open"></i> <?php the_category( ', ' ); ?></span>
									<span><i class="fa fa-tags"></i> <?php the_tags('', ', '); ?></span>
									<span><i class="fa fa-comments"></i> <?php comments_number( 'no comments', 'one comment', '% comments' ); ?></span>
								</h3>
							</section>
						</section>	
						<section class="article-comments">
							<?php comments_template(); ?>
						</section>
					</article>
				<!--End Post Content-->
			</aside>
		<!--End Post List-->
		<!--Start Sidebar-->
			<aside class="post-sidebar">
				<?php get_sidebar(); ?>
			</aside>
		<!--End Sidebar-->
	</section>
	<?php endwhile; else: ?>
        <?php get_template_part('partials/template', 'error'); ?>
    <?php endif; ?>
    <?php get_template_part('partials/content', 'pagination'); ?>
<?php get_footer(); ?>
