$(document).ready(function() {
	$('.container').fitVids();
	$('.has-sub-nav').hover(function() {
		$(this).find('.drop-down').slideDown();
	}, function(){
    	$(this).find('.drop-down').slideUp();
	});
	$('.has-sub-nav > a').click(function(e) {
		e.preventDefault();
	});
});
var headerHeight = $('header.main').outerHeight();
$(window).scroll(function() {
	if($(this).scrollTop() > 100) {
		$('header.main').addClass('fixed').hide();
		$('.container.main').css('margin-top', headerHeight);
	} else {
		$('header.main').removeClass('fixed').show();
		$('.container.main').css('margin-top', 0);
	}
});
