<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<link href='http://fonts.googleapis.com/css?family=Lato:300,700,300italic,700italic|Montserrat' rel='stylesheet' type='text/css'>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/lib/modernizr-latest.js" type="text/javascript"></script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5089c16f5cedcc91"></script>
<?php get_template_part('includes/build/build', 'meta'); ?>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width"/>  
<title><?php wp_title('|'); ?></title>
<?php wp_head(); ?>
</head>
<body>
<section class="page-content">
	<section class="footer-fix">
		<?php if(is_single()) { ?>
			<header class="fixed">
		<?php } else { ?>
			<header class="main">
		<?php } ?>
				<section class="container">
					<?php 
						$title = get_bloginfo('name');
						$url = get_bloginfo('url');
					?>
					<?php if(is_front_page()) { ?>
						<h1><a href="<?php echo $url; ?>" title="<?php echo $title; ?>"><?php echo $title; ?></a></h1>
					<?php } else { ?>
						<h2><a href="<?php echo $url; ?>" title="<?php echo $title; ?>"><?php echo $title; ?></a></h2>
					<?php } ?>
					<!--Social Icons-->
						<?php get_template_part('includes/build/build', 'social'); ?>
					<!--End Social Icons-->
					<!--Category List-->
						<ul class="category-list">
							<li class="has-sub-nav">
								<a href="#" id="dd-trigger">Categories</a>
								<ul class="drop-down">
									<?php 
										$args = array(
												'title_li' => ''
											);
										wp_list_categories($args); 
									?>
								</ul>
							</li>
						</ul>
					<!--End Category List-->
				</section>
		</header>
