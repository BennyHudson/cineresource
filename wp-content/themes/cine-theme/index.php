<?php get_header(); ?>
	
	<section class="container main">
		<!--Start Post List-->
			<aside class="post-list">
				<!--Start Post Content-->
					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<article>
						<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail('full'); ?></a>
						<section class="article-content">
							<h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
							<h3 class="icons">
								<span><i class="fa fa-pencil"></i> <?php the_author(); ?></span>
								<span><i class="fa fa-calendar-o"></i> <?php the_time('d.m.y'); ?></span>
							</h3>
							<section class="article-excerpt">
								<?php the_excerpt(); ?>
							</section>
							<section class="article-meta">
								<h3 class="icons">
									<span><i class="fa fa-folder-open"></i> <?php the_category( ', ' ); ?></span>
									<span><i class="fa fa-tags"></i> <?php the_tags('', ', '); ?></span>
									<span><i class="fa fa-comments"></i> <?php comments_number( 'no comments', 'one comment', '% comments' ); ?></span>
									<span><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><i class="fa fa-arrow-circle-right"></i> Read More</a></span>
								</h3>
							</section>
						</section>	
					</article>
					<?php endwhile; else: ?>
		                <?php get_template_part('partials/template', 'error'); ?>
		            <?php endif; ?>
		            <?php get_template_part('partials/content', 'pagination'); ?>
				<!--End Post Content-->
			</aside>
		<!--End Post List-->
		<!--Start Sidebar-->
			<aside class="post-sidebar">
				<?php get_sidebar(); ?>
			</aside>
		<!--End Sidebar-->
	</section>

<?php get_footer(); ?>
