<?php

	add_filter( 'avatar_defaults', 'crunchifygravatar' );
 
	function crunchifygravatar ($avatar_defaults) {
		$myavatar = get_bloginfo('template_directory') . '/images/cr-avatar_v3.png';
		$avatar_defaults[$myavatar] = "CineResource Avatar";
		return $avatar_defaults;
	}
	function mytheme_init() {
		add_filter('comment_form_defaults','mytheme_comments_form_defaults');
	}
	add_action('after_setup_theme','mytheme_init');

	function mytheme_comments_form_defaults($default) {
		unset($default['comment_notes_after']);
		return $default;
	}

	// First, create a function that includes the path to your favicon
	function add_favicon() {
	  	$favicon_url = get_stylesheet_directory_uri() . '/images/favicons/favicon.ico';
		echo '<link rel="shortcut icon" href="' . $favicon_url . '" />';
	}
	  
	// Now, just make sure that function runs when you're on the login page and admin pages  
	add_action('login_head', 'add_favicon');
	add_action('admin_head', 'add_favicon');

	function my_theme_add_editor_styles() {
    	add_editor_style( 'admin.css' );
	}
	add_action( 'admin_init', 'my_theme_add_editor_styles' );

	function wpb_imagelink_setup() {
		$image_set = get_option( 'image_default_link_type' );
		
		if ($image_set !== 'none') {
			update_option('image_default_link_type', 'none');
		}
	}
	add_action('admin_init', 'wpb_imagelink_setup', 10);

	add_theme_support( 'post-thumbnails' ); 
	function custom_excerpt_length( $length ) {
		return 65;
	}
	add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );
	function new_excerpt_more( $more ) {
		return '';
	}
	add_filter('excerpt_more', 'new_excerpt_more');
	// Replace Wordpress Logo on login screen

	function my_login_logo() { 
		?>
		    <style type="text/css">
		        body.login div#login h1 a {
		            background-image: url(<?php echo get_bloginfo( 'template_directory' ) ?>/images/large-logo.png);
		            display: block;
		            background-size: contain;
		        }
		    </style>
		<?php 
	}
	add_action( 'login_enqueue_scripts', 'my_login_logo' );
	function my_login_logo_url() {
	    return get_bloginfo( 'url' );
	}
	add_filter( 'login_headerurl', 'my_login_logo_url' );

	// Remove Wordpress logo from admin bar

	function annointed_admin_bar_remove() {
        global $wp_admin_bar;

        /* Remove their stuff */
        $wp_admin_bar->remove_menu('wp-logo');
	}
	add_action('wp_before_admin_bar_render', 'annointed_admin_bar_remove', 0);

	function wpbeginner_numeric_posts_nav() {

	if( is_singular() )
		return;

	global $wp_query;

	/** Stop execution if there's only 1 page */
	if( $wp_query->max_num_pages <= 1 )
		return;

	$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
	$max   = intval( $wp_query->max_num_pages );

	/**	Add current page to the array */
	if ( $paged >= 1 )
		$links[] = $paged;

	/**	Add the pages around the current page to the array */
	if ( $paged >= 3 ) {
		$links[] = $paged - 1;
		$links[] = $paged - 2;
	}

	if ( ( $paged + 2 ) <= $max ) {
		$links[] = $paged + 2;
		$links[] = $paged + 1;
	}

	echo '<div class="navigation"><ul>' . "\n";

	/**	Next Post Link */
	if ( get_previous_posts_link() )
		printf( '<li>%s</li>' . "\n", get_previous_posts_link('<') );


	/**	Link to first page, plus ellipses if necessary */
	if ( ! in_array( 1, $links ) ) {
		$class = 1 == $paged ? ' class="active"' : '';

		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

		if ( ! in_array( 2, $links ) )
			echo '<li>…</li>';
	}

	/**	Link to current page, plus 2 pages in either direction if necessary */
	sort( $links );
	foreach ( (array) $links as $link ) {
		$class = $paged == $link ? ' class="active"' : '';
		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
	}

	/**	Link to last page, plus ellipses if necessary */
	if ( ! in_array( $max, $links ) ) {
		if ( ! in_array( $max - 1, $links ) )
			echo '<li>…</li>' . "\n";

		$class = $paged == $max ? ' class="active"' : '';
		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
	}

	/**	Previous Post Link */
	if ( get_next_posts_link() )
		printf( '<li>%s</li>' . "\n", get_next_posts_link('>') );

	echo '</ul></div>' . "\n";

}

function mytheme_comment($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment; ?>
   	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
	   	<aside class="avatar">
	   		<?php echo get_avatar($comment,$size='80' ); ?>
	   	</aside>
	   	<aside class="comment">
	   		<h3><?php printf(__('%s'), get_comment_author_link()) ?></h3>
	   		<h4><i class="fa fa-calendar-o"></i> <?php printf(__('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?> <?php edit_comment_link(__('(Edit)'),'  ','') ?></h4>
	   		<?php if ($comment->comment_approved == '0') : ?>
		    	<p><em><?php _e('Your comment is awaiting moderation.') ?></em></p>
		    <?php endif; ?>
		    <?php comment_text() ?>
		    <span class="reply-trigger"><?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?></span>
	   	</aside>
<?php
        }

?>
